import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ArticleService } from 'src/app/admin/article.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Article } from 'src/app/models/article';
//import { EventEmitter } from 'stream';

@Component({
  selector: 'app-article-summary',
  templateUrl: './article-summary.component.html',
  styleUrls: ['./article-summary.component.css']
})
export class ArticleSummaryComponent implements OnInit {
 isWaitingForServerResponse = false;
 error = '';
 @Output() deleteSuccess = new EventEmitter<boolean>();
  @Input() 
  article!: Article;
  isInEditMode = false;
  constructor(private articleService: ArticleService, public authService: AuthService) { }

  ngOnInit(): void {
  }

  

delete(article: Article) {
     this.isWaitingForServerResponse = true;
     this.articleService.deleteArticle(article).pipe(
       catchError(this.handleError)
     ).subscribe(
       data => {
         this.isWaitingForServerResponse = false;
         this.handleSuccess(data);
       },
       err => {
         this.isWaitingForServerResponse = false;
         this.handleError(err)
       }
     );
}

handleError(err: any) {
  this.error =  err;
  return throwError(this.error)
}

handleSuccess(data: any) {
  this.deleteSuccess.emit(true);
  
}

toggleReadMode() {
  this.isInEditMode =  !this.isInEditMode;

}
}
