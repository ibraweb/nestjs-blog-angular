import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
   registerForm!: FormGroup;
   error = null;
   response$ = null;

  constructor(private formBuilder: FormBuilder, private readonly authService: AuthService) { }

  ngOnInit(): void {
    this.initFormRegister();
  }


  initFormRegister() {
    this.registerForm = this.formBuilder.group({
      email: this.formBuilder.control('', [Validators.required, Validators.email]),
      password: this.formBuilder.control('', [Validators.required, Validators.minLength(5)])
    })
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  onSubmit() {
    this.error = null;
    console.log(this.registerForm.value);
    this.authService.register(this.registerForm.value);

  }
}
