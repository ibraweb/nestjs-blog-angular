import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArticleService } from '../article.service';
import { catchError } from 'rxjs/operators'
import { EMPTY } from 'rxjs'

@Component({
  selector: 'app-article-new',
  templateUrl: './article-new.component.html',
  styleUrls: ['./article-new.component.css']
})
export class ArticleNewComponent implements OnInit {
    
  articleForm!: FormGroup;
  response$: any = null;
  error = null;

  constructor(private formBuilder: FormBuilder, private articleService: ArticleService) { }

  ngOnInit(): void {
    this.initArticle();
  }

  initArticle(){
    this.articleForm = this.formBuilder.group( {
      title: this.formBuilder.control('', [Validators.required, Validators.minLength(3)]),
      content: this.formBuilder.control('', [Validators.required, Validators.minLength(20)]),
       creationDate: new Date().toISOString()
   
    })

  }

  async onSubmit() {

    this.response$ =  await this.articleService.createArticle(this.articleForm.value)
    .pipe(
      catchError(error => {
                     this.error = error;
                     return EMPTY;
                     
      })
      
    );

  }

  get title() {
    return this.articleForm.get('title');
  }


  get content() {
    return this.articleForm.get('content');
  }
}
