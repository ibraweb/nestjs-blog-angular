import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EMPTY } from 'rxjs/internal/observable/empty';
import { catchError } from 'rxjs/internal/operators/catchError';
import { Article } from 'src/app/models/article';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css']
})
export class ArticleEditComponent implements OnInit {
   @Input() article!: Article;
   articleForm!: FormGroup;
   response$: any = null;
   error = null;
  constructor(private formBuilder: FormBuilder, private articleService: ArticleService) { }

  ngOnInit(): void {
        this.initArticle();
  }

  initArticle(){
    this.articleForm = this.formBuilder.group( {
      title: this.formBuilder.control('', [Validators.required, Validators.minLength(3)]),
      content: this.formBuilder.control('', [Validators.required, Validators.minLength(20)]),
       creationDate: new Date().toISOString()
    })
  }

  get title() {
    return this.articleForm.get('title');
  }

  get content() {
    return this.articleForm.get('content');
  }

  async onSubmit() {
    this.error = null;
    //id =  this.article._id;

    this.response$ =  await this.articleService.updateArticle(this.article._id, this.articleForm.value)
    .pipe(
      catchError(error => {
                     this.error = error;
                     return EMPTY;
                     
      })
      
    );

  }



}
